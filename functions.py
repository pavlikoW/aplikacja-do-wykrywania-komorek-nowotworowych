import cv2                 # working with, mainly resizing, images
import numpy as np         # dealing with arrays
import os                  # dealing with directories
from random import shuffle # mixing up or currently ordered data that might lead our network astray in training.
from tqdm import tqdm
import dicom
import matplotlib as plt
from PIL import Image
import matplotlib.image as mpimg

TRAIN_DIR = './dicom/all/'
TEST_DIR = './test/'
IMG_SIZE = 50
LR = 5e-4

MODEL_NAME = 'cancer-{}-{}-{}.model'.format('A1', IMG_SIZE,'raw')


def label_img(img):
    word_label = img.split('.')[0]
    
    if word_label == 'no_cancer': return [1,0]
   
    elif word_label == 'cancer': return [0,1]

def create_train_data():
    training_data = []
    for img in tqdm(os.listdir(TRAIN_DIR)):
        try:
            label = label_img(img)
            path = os.path.join(TRAIN_DIR,img)
            img = dicom.read_file(path,force = True)
            img = np.array(img.pixel_array)
            img = img.reshape([img.shape[1], img.shape[2], 3])
            img = (img[:,:,:3] * [0.2989, 0.5870, 0.1140]).sum(axis=2)
            
            img = cv2.resize((img),(IMG_SIZE,IMG_SIZE))
            training_data.append([np.array(img),np.array(label)])
            img = cv2.flip(img,1)
            training_data.append([np.array(img),np.array(label)])
        except TypeError as e:
            pass
    shuffle(training_data)
    name = ('train_data-{}-{}.npy').format(IMG_SIZE,LR)
    print(len(training_data))
    
    np.save(name, training_data)
    return training_data

def process_test_data():
    testing_data = []
    img = dicom.read_file(fileName)
    img = np.array(img.pixel_array)
    img = img.reshape([img.shape[1], img.shape[2], 3])
    img = (img[:,:,:3] * [0.2989, 0.5870, 0.1140]).sum(axis=2)            
    img = cv2.resize((img),(IMG_SIZE,IMG_SIZE))
    img = ([np.array(img)])

    return img








from dialog_save import App_save
from dialog_open import App_open
from popup import Popup
import sys
from PyQt5.QtGui import QPixmap
import cv2
import numpy as np
import os
import dicom
import tensorflow as tf
import tflearn
from tflearn.layers.conv import conv_2d, max_pool_2d
from tflearn.layers.core import input_data, dropout, fully_connected
from tflearn.layers.estimator import regression
from PyQt5 import QtCore, QtGui, QtWidgets


class UiMainWindow(object):
    pixmap = ''
    IMG_SIZE = 50
    LR = 2e-3
    base_model = ''
    result = 0.3
    openedFile = ''

    def show_result(self):
        print(UiMainWindow.result)
        self.lcdNumber.display(UiMainWindow.result)

    def test_base(self):
        testing_data = []
        if UiMainWindow.base_model != '' and UiMainWindow.openedFile != '':
            print(UiMainWindow.base_model)
            tf.reset_default_graph()
            convnet = input_data(shape=[None, UiMainWindow.IMG_SIZE, UiMainWindow.IMG_SIZE, 1], name='input')

            convnet = conv_2d(convnet, 32, 5, activation='relu')
            convnet = max_pool_2d(convnet, 5)

            convnet = conv_2d(convnet, 64, 5, activation='relu')
            convnet = max_pool_2d(convnet, 5)

            convnet = conv_2d(convnet, 32, 5, activation='relu')
            convnet = max_pool_2d(convnet, 5)

            convnet = conv_2d(convnet, 64, 5, activation='relu')
            convnet = max_pool_2d(convnet, 5)

            convnet = conv_2d(convnet, 32, 5, activation='relu')
            convnet = max_pool_2d(convnet, 5)

            convnet = fully_connected(convnet, 1024, activation='relu')
            convnet = dropout(convnet, 0.8)

            convnet = fully_connected(convnet, 2, activation='softmax')
            convnet = regression(convnet, optimizer='adam', learning_rate=UiMainWindow.LR, loss='categorical_crossentropy',
                                 name='targets')

            model = tflearn.DNN(convnet, tensorboard_dir='log')

            if os.path.exists('./models/'+UiMainWindow.base_model+'.meta'):
                model.load(UiMainWindow.base_model)
                print('Model zaladowany')
                if UiMainWindow.base_model == 'models/cancer-basic.model':
                    img = dicom.read_file(UiMainWindow.openedFile)
                    img = img.pixel_array
                    img = np.array(img)
                    img = img.reshape([img.shape[1], img.shape[2], 3])
                    img = (img[:, :, :3] * [0.2989, 0.5870, 0.1140]).sum(axis=2)
                if UiMainWindow.base_model == 'models/cancer-dyl.model':
                    img = dicom.read_file(UiMainWindow.openedFile)
                    img = img.pixel_array
                    img = np.array(img)
                    img = img.reshape([img.shape[1], img.shape[2], 3])
                    img = (img[:, :, :3] * [0.2989, 0.5870, 0.1140]).sum(axis=2)
                    kernel = np.ones((4, 4), np.uint16)
                    img = cv2.dilate(img, kernel, iterations=1)
                if UiMainWindow.base_model == 'models/cancer-ero.model':
                    img = dicom.read_file(UiMainWindow.openedFile)
                    img = img.pixel_array
                    img = np.array(img)
                    img = img.reshape([img.shape[1], img.shape[2], 3])
                    img = (img[:, :, :3] * [0.2989, 0.5870, 0.1140]).sum(axis=2)
                    kernel = np.ones((4, 4), np.uint16)
                    img = cv2.erode(img, kernel, iterations=1)
                if UiMainWindow.base_model == 'models/cancer-otw.model':
                    img = dicom.read_file(UiMainWindow.openedFile)
                    img = img.pixel_array
                    img = np.array(img)
                    img = img.reshape([img.shape[1], img.shape[2], 3])
                    img = (img[:, :, :3] * [0.2989, 0.5870, 0.1140]).sum(axis=2)
                    kernel = np.ones((6, 6), np.uint16)
                    img = cv2.morphologyEx(img, cv2.MORPH_OPEN, kernel)
                if UiMainWindow.base_model == 'models/cancer-dom.model':
                    img = dicom.read_file(UiMainWindow.openedFile)
                    img = img.pixel_array
                    img = np.array(img)
                    img = img.reshape([img.shape[1], img.shape[2], 3])
                    img = (img[:, :, :3] * [0.2989, 0.5870, 0.1140]).sum(axis=2)
                    kernel = np.ones((2, 2), np.uint16)
                    img = cv2.morphologyEx(img, cv2.MORPH_CLOSE, kernel)
            else:
                print('Brak modelu')

            img = cv2.resize((img), (50, 50))
            testing_data.append([np.array(img)])
            for data in testing_data:
                data = data[0]
                data = data.reshape(50, 50, 1)
                model_out = model.predict([data])[0]
                model_out = model_out[1]
                print(model_out)
                if model_out < 0.001:
                    model_out = 0.01
                print(model_out)
                print(float(model_out))
                model_out = (str(model_out)[:4])
                UiMainWindow.result = float(model_out)
                print(UiMainWindow.result)
            self.show_result()

    def read_base_raw(self):
        UiMainWindow.base_model = 'models/cancer-basic.model'

    def read_base_dyl(self):
        UiMainWindow.base_model = 'models/cancer-dyl.model'

    def read_base_ero(self):
        UiMainWindow.base_model = 'models/cancer-ero.model'

    def read_base_otw(self):
        UiMainWindow.base_model = 'models/cancer-otw.model'

    def read_base_dom(self):
        UiMainWindow.base_model = 'models/cancer-dom.model'

    def save_dialog(self):
        self.window = QtWidgets.QMainWindow()
        self.ui = App_save()

    def open_dialog(self):
        self.window = QtWidgets.QMainWindow()
        self.ui = App_open()
        print(App_open.fileName)
        if App_open.fileName and App_open.fileName[-3:]== 'dcm':
            img = App_open.fileName
            img = dicom.read_file(img)
            img = img.pixel_array
            img = np.array(img)
            if len(img.shape)== 3:
                    img = img.reshape([img.shape[1], img.shape[2], 3])
                    img = (img[:,:,:3] * [0.2989, 0.5870, 0.1140]).sum(axis=2)
                    img = np.array(img*255 , dtype = np.uint16)

            cv2.imwrite("1.png", img)
            cv2.imwrite("2.png", img)
            pixmap = QPixmap('1.png')
            UiMainWindow.pixmap = pixmap
            self.label_dicom.setPixmap(pixmap)
            self.label_dicom.setScaledContents(True)
            UiMainWindow.openedFile = App_open.fileName
            App_open.fileName = False

    def morph(self,c):

        if Popup.kernel:
            a = int(Popup.kernel)
            b = int(Popup.kernel)
            img = cv2.imread('2.png')
            kernel = np.ones((a, b), np.uint16)
            if c == 'dyl':
                transformed_img = cv2.dilate(img, kernel, iterations=1)
            elif c == 'ero':
                transformed_img = cv2.erode(img, kernel, iterations=1)
            elif c == 'otw':
                transformed_img = cv2.morphologyEx(img, cv2.MORPH_OPEN, kernel)
            elif c == 'dom':
                transformed_img = cv2.morphologyEx(img, cv2.MORPH_CLOSE, kernel)

            cv2.imwrite('2.png', transformed_img)
            pixmap = QPixmap('2.png')
            self.label_dicom.setPixmap(pixmap)
            self.label_dicom.setScaledContents(True)
            Popup.kernel = False
        else:
            pass

    def button_dyl(self):
        if os.path.exists('2.png') and UiMainWindow.pixmap:
            self.window = QtWidgets.QMainWindow()
            self.ui = Popup()
            self.morph('dyl')

    def button_ero(self):
        if os.path.exists('2.png') and UiMainWindow.pixmap:
            self.window = QtWidgets.QMainWindow()
            self.ui = Popup()
            self.morph('ero')

    def button_otw(self):
        if os.path.exists('2.png') and UiMainWindow.pixmap:
            self.window = QtWidgets.QMainWindow()
            self.ui = Popup()
            self.morph('otw')

    def button_dom(self):
        if os.path.exists('2.png') and UiMainWindow.pixmap:
            self.window = QtWidgets.QMainWindow()
            self.ui = Popup()
            self.morph('dom')

    def button_res(self):
        if os.path.exists('2.png') and UiMainWindow.pixmap:
            img = cv2.imread('1.png')
            cv2.imwrite("2.png", img)
            pixmap = QPixmap('2.png')
            self.label_dicom.setPixmap(pixmap)
            self.label_dicom.setScaledContents(True)

    def setupui(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(640, 560)
        MainWindow.setMaximumSize(QtCore.QSize(640, 560))
        font = QtGui.QFont()
        font.setFamily("Calibri")
        font.setPointSize(10)
        MainWindow.setFont(font)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.centralwidget.sizePolicy().hasHeightForWidth())
        self.centralwidget.setSizePolicy(sizePolicy)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayoutWidget_2 = QtWidgets.QWidget(self.centralwidget)
        self.verticalLayoutWidget_2.setGeometry(QtCore.QRect(10, 260, 100, 165))
        self.verticalLayoutWidget_2.setObjectName("verticalLayoutWidget_2")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.verticalLayoutWidget_2)
        self.verticalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.label_2 = QtWidgets.QLabel(self.verticalLayoutWidget_2)
        self.label_2.setObjectName("label_2")
        self.verticalLayout_2.addWidget(self.label_2)
        self.pushButton_Dyl = QtWidgets.QPushButton(self.verticalLayoutWidget_2)
        self.pushButton_Dyl.setObjectName("pushButton_Dyl")
        self.verticalLayout_2.addWidget(self.pushButton_Dyl)
        self.pushButton_Ero = QtWidgets.QPushButton(self.verticalLayoutWidget_2)
        self.pushButton_Ero.setObjectName("pushButton_Ero")
        self.verticalLayout_2.addWidget(self.pushButton_Ero)
        self.pushButton_Otw = QtWidgets.QPushButton(self.verticalLayoutWidget_2)
        self.pushButton_Otw.setObjectName("pushButton_Otw")
        self.verticalLayout_2.addWidget(self.pushButton_Otw)
        self.pushButton_Dom = QtWidgets.QPushButton(self.verticalLayoutWidget_2)
        self.pushButton_Dom.setObjectName("pushButton_Dom")
        self.verticalLayout_2.addWidget(self.pushButton_Dom)
        self.pushButton_Res = QtWidgets.QPushButton(self.verticalLayoutWidget_2)
        self.pushButton_Res.setObjectName("pushButton_Res")
        self.verticalLayout_2.addWidget(self.pushButton_Res)
        self.verticalLayoutWidget = QtWidgets.QWidget(self.centralwidget)
        self.verticalLayoutWidget.setGeometry(QtCore.QRect(10, 10, 100, 220))
        self.verticalLayoutWidget.setObjectName("verticalLayoutWidget")
        self.verticalLayout_3 = QtWidgets.QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout_3.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.label = QtWidgets.QLabel(self.verticalLayoutWidget)
        self.label.setObjectName("label")
        self.verticalLayout_3.addWidget(self.label)
        self.radioButton_Pod = QtWidgets.QRadioButton(self.verticalLayoutWidget)
        self.radioButton_Pod.setObjectName("radioButton_Pod")
        self.verticalLayout_3.addWidget(self.radioButton_Pod)
        self.radioButton_Dyl = QtWidgets.QRadioButton(self.verticalLayoutWidget)
        self.radioButton_Dyl.setObjectName("radioButton_Dyl")
        self.verticalLayout_3.addWidget(self.radioButton_Dyl)
        self.radioButton_Ero = QtWidgets.QRadioButton(self.verticalLayoutWidget)
        self.radioButton_Ero.setObjectName("radioButton_Ero")
        self.verticalLayout_3.addWidget(self.radioButton_Ero)
        self.radioButton_Otw = QtWidgets.QRadioButton(self.verticalLayoutWidget)
        self.radioButton_Otw.setObjectName("radioButton_Otw")
        self.verticalLayout_3.addWidget(self.radioButton_Otw)
        self.radioButton_Dom = QtWidgets.QRadioButton(self.verticalLayoutWidget)
        self.radioButton_Dom.setObjectName("radioButton_Dom")
        self.verticalLayout_3.addWidget(self.radioButton_Dom)
        self.pushButton_test = QtWidgets.QPushButton(self.verticalLayoutWidget)
        self.pushButton_test.setObjectName("pushButton_test")
        self.verticalLayout_3.addWidget(self.pushButton_test)
        self.lcdNumber = QtWidgets.QLCDNumber(self.verticalLayoutWidget)
        self.lcdNumber.setObjectName("lcdNumber")
        self.verticalLayout_3.addWidget(self.lcdNumber)
        self.label_dicom = QtWidgets.QLabel(self.centralwidget)
        self.label_dicom.setGeometry(QtCore.QRect(120, 10, 500, 500))
        pixmap = QPixmap('White.png')
        self.label_dicom.setPixmap(pixmap)
        self.label_dicom.setScaledContents(True)
        self.label_dicom.setObjectName("label_dicom")
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 607, 21))
        self.menubar.setObjectName("menubar")
        self.menuPlik = QtWidgets.QMenu(self.menubar)
        self.menuPlik.setObjectName("menuPlik")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.actionOtw_rz = QtWidgets.QAction(MainWindow)
        self.actionOtw_rz.setObjectName("actionOtw_rz")
        self.menuPlik.addAction(self.actionOtw_rz)
        self.menubar.addAction(self.menuPlik.menuAction())
        self.retranslateui(MainWindow)
        self.radioButton_Pod.clicked.connect(self.read_base_raw)
        self.radioButton_Dyl.clicked.connect(self.read_base_dyl)
        self.radioButton_Ero.clicked.connect(self.read_base_ero)
        self.radioButton_Otw.pressed.connect(self.read_base_otw)
        self.radioButton_Dom.clicked.connect(self.read_base_dom)
        self.pushButton_test.clicked.connect(self.test_base)
        self.actionOtw_rz.triggered.connect(self.open_dialog)
        self.pushButton_Dyl.clicked.connect(self.button_dyl)
        self.pushButton_Ero.clicked.connect(self.button_ero)
        self.pushButton_Otw.clicked.connect(self.button_otw)
        self.pushButton_Dom.clicked.connect(self.button_dom)
        self.pushButton_Res.clicked.connect(self.button_res)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

        self.centralwidget.setStyleSheet("""
        .QWidget {

            background-color: rgb(240, 245, 255);
            }

        .QPushButton {
            background-color: #4CAF50; /* Green */
            border: none;
            color: white;
            padding: 1px 2px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 14px;
            border-radius: 6px;
            }

        .QLCDNumber{
            background-color: #226600;
            border-radius: 2px;
            color: white;
            padding: 1px 2px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 16px;
            }

        .QRadioButton{
            background-color: #4CAF50; /* Green */
            border: none;
            color: white;
            padding: 1px 2px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 14px;
            border-radius: 6px;
            }
        """)

    def retranslateui(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.label_2.setText(_translate("MainWindow", "Transformacja"))
        self.pushButton_Dyl.setText(_translate("MainWindow", "Dylatacja"))
        self.pushButton_Ero.setText(_translate("MainWindow", "Erozja"))
        self.pushButton_Otw.setText(_translate("MainWindow", "Otwarcie"))
        self.pushButton_Dom.setText(_translate("MainWindow", "Domknięcie"))
        self.pushButton_Res.setText(_translate("MainWindow", "Reset"))
        self.label.setText(_translate("MainWindow", "Rodzaj bazy"))
        self.radioButton_Pod.setText(_translate("MainWindow", "Podstawowa "))
        self.radioButton_Dyl.setText(_translate("MainWindow", "Dylatacja"))
        self.radioButton_Ero.setText(_translate("MainWindow", "Erozja"))
        self.radioButton_Otw.setText(_translate("MainWindow", "Otwarcie"))
        self.radioButton_Dom.setText(_translate("MainWindow", "Domknięcie"))
        self.pushButton_test.setText(_translate("MainWindow", "Test"))
        self.menuPlik.setTitle(_translate("MainWindow", "Plik"))
        self.actionOtw_rz.setText(_translate("MainWindow", "Otwórz"))


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = UiMainWindow()
    ui.setupui(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

from PyQt5.QtWidgets import QWidget,QInputDialog, QApplication
import sys


class Popup(QWidget):
    kernel = ''

    def __init__(self):
        super().__init__()
        self.showDialog()
        
    def show_dialog(self):
        global kernel
        kernel, ok = QInputDialog.getText(self, 'Kernel', 'Ustaw kernel:')

        if ok:
            Popup.kernel = kernel


if __name__ == '__main__':
    
    app = QApplication(sys.argv)
    ex = Popup()
    sys.exit(app.exec_())

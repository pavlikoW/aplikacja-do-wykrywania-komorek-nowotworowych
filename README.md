Aplikacja pozwalająca na wykrywanie komórek nowotworowych na skanach rezonansu magnetycznego mózgu.
Aplikacja została napisana w języku Python, za proces klasyfikacji odpowiadają konwolucyjne sieci neuronowe 2D.
Główny plik apliakcji - dicomapp.py

import cv2                 # working with, mainly resizing, images
import numpy as np         # dealing with arrays
import os                  # dealing with directories
from random import shuffle # mixing up or currently ordered data that might lead our network astray in training.
from tqdm import tqdm
import dicom
import matplotlib.pyplot as plt
from PIL import Image
import matplotlib.image as mpimg
import numpy

def open_image():
    print(fileName)
    
    img = dicom.read_file(fileName)
    img = (img.pixel_array)
    img = np.array(img)
    print(img)
    
    if len(img.shape)== 3:
        img = img.reshape([img.shape[1], img.shape[2], 3])
        img = (img[:,:,:3] * [0.2989, 0.5870, 0.1140]).sum(axis=2)
        img = np.array(img*255 , dtype = np.uint16)

    cv2.imshow('original', img)
    '''

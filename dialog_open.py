import sys
from PyQt5.QtWidgets import QApplication, QWidget, QInputDialog, QLineEdit, QFileDialog
from PyQt5.QtGui import QIcon
import cv2                 # working with, mainly resizing, images
import numpy as np         # dealing with arrays
import os                  # dealing with directories
from random import shuffle # mixing up or currently ordered data that might lead our network astray in training.
import dicom
import matplotlib.pyplot as plt
from PIL import Image
import matplotlib.image as mpimg
import numpy


class App_open(QWidget):
    fileName = ''
    def __init__(self):
        super().__init__()
        self.title = 'PyQt5 file dialogs - pythonspot.com'
        self.left = 10
        self.top = 10
        self.width = 640
        self.height = 480
        self.initui()


    def initui(self,):
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)
        self.openfilenmedialog()
 
    def openfilenamedialog(self):
        global fileName
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        fileName, _ = QFileDialog.getOpenFileName(self,"QFileDialog.getOpenFileName()", "","All Files (*);;Python Files (*.py)", options=options)
        if fileName:
            '''
            print(fileName)
            img = dicom.read_file(fileName)
            img = (img.pixel_array)
            img = np.array(img)
        
            if len(img.shape)== 3:
                img = img.reshape([img.shape[1], img.shape[2], 3])
                img = (img[:,:,:3] * [0.2989, 0.5870, 0.1140]).sum(axis=2)
                img = np.array(img*255 , dtype = np.uint16)
            
            cv2.imshow('original', img)
            '''
            App_open.fileName = fileName
    def openfileNamesdialog(self):
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        files, _ = QFileDialog.getOpenFileNames(self,"QFileDialog.getopenfileNames()", "","All Files (*);;Python Files (*.py)", options=options)
        if files:
            print(files)
 
    def savefiledialog(self):
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        fileName, _ = QFileDialog.getSaveFileName(self,"QFileDialog.getSaveFileName()","","All Files (*);;Text Files (*.txt)", options=options)
        if fileName:
            print(fileName)
 
if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = App_open()
    sys.exit(app.exec_())

